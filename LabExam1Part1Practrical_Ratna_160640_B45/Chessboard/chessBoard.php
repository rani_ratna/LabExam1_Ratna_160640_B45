<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chessboard Using HTML and CSS</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <div class="header template">
        <h2>Draw a Chessboard using HTML and CSS</h2>
    </div>
    <div class="maincontent template">
        <div class="chessboard">
            <!-----1st row------>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <!-----2nd row------>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <!-----3rd row------>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <!-----4th row------>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <!-----5th row------>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <!-----6th row------>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <!-----7th row------>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <!-----8th row------>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>
            <div class="black"></div>
            <div class="white"></div>

        </div>

    </div>
    <div class="footer template">
        <p>Copyright@............</p>
    </div>

</body>
</html>



