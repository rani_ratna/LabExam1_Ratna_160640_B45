

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chessboard</title>
    <link rel="stylesheet" href="style_chessboard.css">

    </head>
<body>

<form action="" method="post" name="showform">
    <div class="topsection">
        <h2>Chessboard</h2>
        <input type="text" name="txtDimension" placeholder="Type the Dimension" required="required">
        <input type="submit" value="Submit" name="submit">
    </div>

</form>
</body>
</html>

<?php

if(isset($_POST['txtDimension']))

{
    $dimension= $_POST['txtDimension'];
    showChessBoard();

}

function showChessBoard(){
    $a=$GLOBALS['dimension'];
    $heightandwidth=400/$a;
    echo "<div class='container'>";
    echo "<table align='center'>";
    for ($row=1;$row<=$a;$row++)
    {
        echo "<tr>";
        for($col=1;$col<=$a;$col++)
        {
            $total=$row+$col;
            if($total%2==0)
            {
                echo "<td height=$heightandwidth width=$heightandwidth bgcolor=#FFFFFF></td>";
            }
            else
            {
                echo "<td height=$heightandwidth width=$heightandwidth bgcolor=#000000></td>";
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    echo "</div>";

}

?>



